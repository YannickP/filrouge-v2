# Kubernetes on Cloud
Fork of Jean-Pierre Messager: https://framagit.org/jpython/k8s-on-cloud

Deploy K8s on AWS

![](Archi_K8S.png)

# Install Git, Make et jq

```
apt install git
apt install make
apt install jq
```

# Deploy EC2 virtual machines with Terraform

## Configure your aws local user configuration

Create your `.aws` personal configuration directory where you will put two files.

`~/.aws/config`

```
[default]
region=us-east-2c
output=json
```

Create a new user on the AWS portal with Administrator Access and Copy/Paste your keys in the credentials file.

`~/.aws/credentials`

```
[default]
aws_access_key_id=YOUR ACCESS KEY
aws_secret_access_key=YOUR SECRET ACCESS KEY
``` 

Make sure only YOU can read that file: `chmod a=rw,go= ~/.aws/credentials`


We are suggesting us-east-2c as the AMI, we will be using is only available there, anyway
it is very likely than any reasonable Debian 9/10 AMI, or a recent CentOS will do (you
may have to adapt Terraform files in this case).

## Install and run Terraform

```
git clone https://framagit.org/YannickP/filrouge-v2.git
```

Install terraform (last version, or v0.12.18 this procedure has been tested with) and
put terraform binary in a directory which is in your PATH.

```
unzip terraform
cp terraform /usr/local/bin
```

Get the files from this repository, build files and initialize Terraform, then construct
your EC2 infrastructure:

```
cd k8s-on-cloud/
cd ssh-keys/
./generate_keys 
cd ../Scripts/
make
cd ../Tf/
terraform init
terraform plan
terraform apply
```

If every went well you should be able to connect to your bastion ec2 instance:

```
$ ./go_bastion
Bastion public ip: 3.15.139.236
Warning: Permanently added '3.15.139.236' (ECDSA) to the list of known hosts.
Linux ip-10-0-0-42 4.9.0-11-amd64 #1 SMP Debian 4.9.189-3 (2019-09-02) x86_64
...
admin@ip-10-0-0-42:~$ 
```

You first have to wait for cloud-init to have finished, at that time a
file `/tmp/cloud-init-bastion-ok` will appear. If you get bored you can
run this waiting for the cloud-init process to disappear :

```
admin@ip-10-0-0-42:~$ watch pstree
```

Then you'll have to wait (again!) for all nodes to be properly setted up:

```
admin@ip-10-0-0-42:~$ watch ./check_node_ci 50 100 101 102 
```

When you'll see a file `/tmp/cloud-init-node-ok` at all nodes you can move forward.

# Deploy Kubernetes with an Ansible Playbook

Still logged in your bastion virtual machine, clone another repository which
contains the playbook and run it:

```
admin@ip-10-0-0-42:~$ git clone https://framagit.org/jpython/kubeadm-ansible.git
admin@ip-10-0-0-42:~$ cd kubeadm-ansible/
admin@ip-10-0-0-42:~/kubeadm-ansible$ ansible-playbook site.yaml 
```

Check that Kubernetes cluster is up and running:

```
admin@ip-10-0-0-42:~/kubeadm-ansible$ ssh root@10.0.0.50
root@ip-10-0-0-50:~# kubectl get nodes
NAME            STATUS   ROLES    AGE   VERSION
ip-10-0-0-100   Ready    <none>   33m   v1.17.0
ip-10-0-0-101   Ready    <none>   33m   v1.17.0
ip-10-0-0-102   Ready    <none>   33m   v1.17.0
ip-10-0-0-50    Ready    master   33m   v1.17.0

root@ip-10-0-0-50:~# kubectl get pods -n kube-system
NAME                                      READY   STATUS    RESTARTS   AGE
calico-kube-controllers-55754f75c-wvkbw   1/1     Running   0          3h37m
calico-node-4sczw                         1/1     Running   0          3h37m
calico-node-75xbj                         1/1     Running   0          3h37m
calico-node-hqngl                         1/1     Running   0          3h37m
calico-node-mz7v5                         1/1     Running   0          3h37m
coredns-6955765f44-knvfh                  1/1     Running   0          3h37m
coredns-6955765f44-pxj5p                  1/1     Running   0          3h37m
etcd-ip-10-0-0-50                         1/1     Running   0          3h38m
kube-apiserver-ip-10-0-0-50               1/1     Running   0          3h38m
kube-controller-manager-ip-10-0-0-50      1/1     Running   0          3h38m
kube-proxy-fj97j                          1/1     Running   0          3h37m
kube-proxy-jj8nw                          1/1     Running   0          3h37m
kube-proxy-mcn5r                          1/1     Running   0          3h37m
kube-proxy-xdhwh                          1/1     Running   0          3h37m
kube-scheduler-ip-10-0-0-50               1/1     Running   0          3h38m

```
## Add a new worker node

- Modify instances.tf to add a new worker virtual machine
- Reapply Terraform configuration, wait for the new vm to finish cloud-init process
- Log to the bastion and update hosts.ini 
- Ask Ansible to set up the new node: `ansible-playbook site.yaml`
- Check with kubectl that the new node is up and running

Files to modify in order to add new workers :

- `hosts.ini` on the bastion Ansible playbook repository (IP corresponding
to the number of worker)

`~/kubeadm-ansible/hosts.ini`

```
[node]
10.0.0.[100:103]
```

- `instances.tf` on your local machine (then Terraform has to be re-applied)
 
`~/.aws/k8s-on-cloud/Tf/instances.tf`

```
variable "workers_count" {
  default = NUMBER OF WORKERS
}
```

Apply changes with Terraform

```
terraform apply
```

You may want to examine `group_vars/all.yml` in the same repository, this is
the Kubernetes cluster configuration.
The playbook can be replayed by using the same command you've ran initially
(`ansible-playbook site.yaml`), the whole cluster can be resetted by runing
`ansible-playbook reset-site.yaml`.

# Deployement

You can test a very simple deployment with helm as root on the master node:

```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get-helm-3 > get_helm.sh
chmod +x get_helm.sh 
./get_helm.sh 
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm search repo bitnami | grep nginx
helm install bitnami/nginx --generate-name 
```

Wait for the pod to start and look for the corresponding IP for the service:

```
# kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
nginx-1578899914-7469d55755-vzjd2   1/1     Running   0          54m

# kubectl get svc
NAME               TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)                      AGE
kubernetes         ClusterIP      10.96.0.1      <none>        443/TCP                      61m
nginx-1578899914   LoadBalancer   10.96.X.Y   <pending>     80:31825/TCP,443:30301/TCP      54m
```

You can test that the nginx default home page is there:
```
# apt install w3m
# w3m http://10.96.X.Y
```

type 'q' to quit w3m, a very convenient console based Web navigator.

You can access your service from anywhere without bothering to get an elastic IP.

- Log on the bastion and configure SSH tunnels, replace 10.96.X.Y with the service IP
you've got above:

```
sudo ssh -L *:80:10.96.X.Y:80 10.0.0.50 -f -N 
sudo ssh -L *:443:10.96.X.Y:443 10.0.0.50 -f -N 
```

If you want stop the nginx service:
Change bellow nginx-name by the nginx name given by `kubectl get svc`

```
helm delete nginx-name
```

To stop such SSH tunnels just pick the ssh client process `ps aux | grep ssh`.
```
kill ID PROCESS
```


## Deploy several apache/wordpress and add a load balancer

On the Master node, download the files contained in the `App` folder.
```
git clone https://framagit.org/YannickP/filrouge-v2.git
```

In the kustomization.yaml, change userpw by the password you want to use:
```
secretGenerator:
- name: mysql-pass
  literals:
  - password=userpw
```

The kustomization.yaml contains all the resources for deploying a WordPress site and a MySQL database. 
You can apply with:
`~/filrouge-v2/App`

```
kubectl apply -k ./
```

To stop the Wordpress and MySQL:
```
kubectl delete -k ./
```


- Verify that the Secret exists by running the following command:
```
kubectl get secrets
```

The response should be like this:

```
  NAME                    TYPE                                  DATA   AGE
  mysql-pass-c57bb4t7mf   Opaque                                1      9s
```

Verify that the Pod is running by running the following command (It can take up to a few minutes for 
the Pod’s Status to be RUNNING.).
```
kubectl get pods
```

Verify that the Service is running by running the following command:
```
kubectl get services wordpress
```
The answer should be like this:
```
NAME        TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
wordpress   LoadBalancer   10.96.80.57   <pending>     80:31011/TCP   4h11m
```

## Access to Wordpress from outside

- Log on the bastion and configure SSH tunnels, replace 10.96.X.Y with the service IP
you've got above:

```
sudo ssh -L *:80:10.96.X.Y:80 10.0.0.50 -f -N 
sudo ssh -L *:443:10.96.X.Y:443 10.0.0.50 -f -N 
```

Check the `IP publique IPv4` from your Bastion on the AWS portal and try on a web browser.

## How to add a new worker and increase the number of apache running?

Look the chapter `Add a new worker node` and  increase the number of worker as you need.

- Increase the replica's number on the `wordpress-deployment.yaml` on the Master node.

```
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: ReplicaSet
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  replicas: 3
```

And apply the `kustomization.yaml`

```
kubectl apply -k ./
```

- Another method consists on increasing (or decreasing) with one command line:

If it's a replicaset:
```
kubectl scale --replicas=1 -f mysql-deployment.yaml
```
If it's a ressource:
```
kubectl scale --replicas=2 rs/wordpress
```

