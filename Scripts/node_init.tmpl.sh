#!/usr/bin/env bash
# Dirty trick to make k8s believe we do have 3 times more memory
# and other weird stuff

FAKEMEMTOTAL=$( grep '^MemTotal:' /proc/meminfo | awk '{print $2 * 3}' )
install -m 644 /proc/meminfo /var/local/meminfo
sed -E -i -e "/^MemTotal:/s/[0-9]+ kB$/${FAKEMEMTOTAL} kB/" /var/local/meminfo
echo "/var/local/meminfo /proc/meminfo inode bind 0 0" >> /etc/fstab
mount -a

install -d -m 700 /root/.ssh
base64 -d > /root/.ssh/authorized_keys <<EOF
PUBKEY
EOF

cat > /etc/apt/apt.conf.d/00proxy <<EOF
Acquire::http::Proxy "http://10.0.0.42:3128/";
Acquire::https::Proxy "http://10.0.0.42:3128/";
EOF

cat > /etc/environment <<EOF
http_proxy=http://10.0.0.42:3128
https_proxy=http://10.0.0.42:3128
EOF

export http_proxy=http://10.0.0.42:3128
export https_proxy=http://10.0.0.42:3128
while true
do
    if wget -qS http://neverssl.com 2>&1 | head -n 1 | grep -q '200 OK$'
    then
        break
    fi
    sleep 5
done

apt -y install dirmngr
cat > /etc/apt/sources.list.d/ansible.list <<EOF
deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main
EOF
apt-key adv --keyserver-options http-proxy=http://10.0.0.42:3128 \
	    --keyserver keyserver.ubuntu.com \
	    --recv-keys 93C4A3FD7BB9C367

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
cat > /etc/apt/sources.list.d/docker.list <<EOF
deb https://download.docker.com/linux/debian stretch stable
EOF

curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat > /etc/apt/sources.list.d/kubernetes.list << EOF
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF

apt -y update
apt -y install apt-transport-https
apt -y install ansible
apt -y install docker-ce

cat > /etc/network/interfaces-off <<EOF
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d
auto lo
iface lo inet loopback
auto eth0
iface eth0 inet dhcp
  post-up ip route del default via 10.0.0.1
  post-up ip route add 169.254.0.0/16 via 10.0.0.1
  post-up ip route add default via 10.0.0.42
allow-hotplug eth0
EOF

ifdown eth0; ifup eth0

apt -y install docker-ce
cat > /lib/systemd/system/docker.service << EOF
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
BindsTo=containerd.service
After=network-online.target firewalld.service containerd.service
Wants=network-online.target
Requires=docker.socket

[Service]
Type=notify
# the default is not to use systemd for cgroups because the delegate issues still
# exists and systemd currently does not support the cgroup feature set required
# for containers run by docker
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
ExecReload=/bin/kill -s HUP $MAINPID
Environment=HTTP_PROXY=http://10.0.0.42:3128
Environment=HTTPS_PROXY=http://10.0.0.42:3128
Environment=http_proxy=http://10.0.0.42:3128
Environment=https_proxy=http://10.0.0.42:3128
TimeoutSec=0
RestartSec=2
Restart=always

# Note that StartLimit* options were moved from "Service" to "Unit" in systemd 229.
# Both the old, and new location are accepted by systemd 229 and up, so using the old location
# to make them work for either version of systemd.
StartLimitBurst=3

# Note that StartLimitInterval was renamed to StartLimitIntervalSec in systemd 230.
# Both the old, and new name are accepted by systemd 230 and up, so using the old name to make
# this option work for either version of systemd.
StartLimitInterval=60s

# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity

# Comment TasksMax if your systemd version does not support it.
# Only systemd 226 and above support this option.
TasksMax=infinity

# set delegate yes so that systemd does not reset the cgroups of docker containers
Delegate=yes

# kill only the docker process, not all processes in the cgroup
KillMode=process

[Install]
WantedBy=multi-user.target
EOF

systemctl restart docker

cat >> /root/.bashrc <<EOF
export KUBECONFIG=/etc/kubernetes/admin.conf
unset http_proxy
unset https_proxy
source /etc/bash_completion
if kubectl version > /dev/null
then
  source <(kubectl completion bash)
fi
EOF

date > /tmp/cloud-init-node-ok

