
resource "aws_key_pair" "admin" {
  key_name   = "admin"
  public_key = file("../ssh-keys/id_rsa_aws.pub")
}

resource "aws_key_pair" "admin-int" {
  key_name   = "admin-int"
  public_key = file("../ssh-keys/id_rsa_aws_int.pub")
}


resource "aws_security_group" "sg_bastion" {
  name = "sg_bastion"
  vpc_id = aws_vpc.vpc_main.id
  ingress {
    from_port = "22"
    to_port   = "22"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = "8001"
    to_port   = "8500"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = "80"
    to_port   = "80"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = "443"
    to_port   = "443"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "bastion" {
  #ami           = "ami-045fa58af83eb0ff4"
  ami		= "ami-0788cc5a8c3155f0f"
  instance_type = "t2.micro"
  key_name      = "admin"
  vpc_security_group_ids = [aws_security_group.sg_bastion.id,
			    aws_security_group.sg_internal.id]
  subnet_id              = aws_subnet.subnet-a.id
  private_ip 		 = "10.0.0.42"
  associate_public_ip_address = "true"
  user_data = file("../Scripts/bastion_init.sh")
  tags = {
    Name = "bastion"
  }
}

resource "aws_security_group" "sg_internal" {
  name = "sg_internal"
  vpc_id = aws_vpc.vpc_main.id
  ingress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    cidr_blocks = ["10.0.0.0/24"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "k8s-master" {
  #ami           = "ami-045fa58af83eb0ff4"
  ami		= "ami-0788cc5a8c3155f0f"
  instance_type = "t2.micro"
  key_name      = "admin-int"
  vpc_security_group_ids = [aws_security_group.sg_internal.id]
  subnet_id              = aws_subnet.subnet-a.id
  private_ip 		 = "10.0.0.50"
  associate_public_ip_address = "true"
  user_data = file("../Scripts/node_init.sh")
  tags = {
    Name = "k8s-master"
    App  = "k8s"
    k8srole = "master"
  }
}

variable "workers_count" {
  default = 3
}

resource "aws_instance" "workers" {
  #ami           = "ami-045fa58af83eb0ff4"
  count         = var.workers_count
  ami		= "ami-0788cc5a8c3155f0f"
  instance_type = "t2.micro"
  key_name      = "admin-int"
  vpc_security_group_ids = [aws_security_group.sg_internal.id]
  subnet_id              = aws_subnet.subnet-a.id
  private_ip 		 = "10.0.0.${count.index + 100}"
  associate_public_ip_address = "true"
  user_data = file("../Scripts/node_init.sh")
  tags = {
    Name = "worker-${count.index + 1}"
    App  = "k8s"
    k8srole = "worker"
  }
}

output "bastion_ip" {
  value = "${aws_instance.bastion.*.public_ip}"
}

output "bastion_internal_ip" {
  value = "${aws_instance.bastion.*.private_ip}"
}

output "k8s_admin_ip" {
  value = "${aws_instance.k8s-master.*.private_ip}"
}

#output "workers-ip" {
#  count = var.workers_count
#  name = "worker-${count.index + 1}-ip"
#  value = "${aws_instance.worker-${count.index -1}.*.private_ip}"
#output "instances-ip" {
#  value = "${aws_instance..private_ip}"
#}
