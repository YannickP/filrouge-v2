# Evolution de la plateforme

## 1. Terraform.tfstate
Terraform.tfstate records the state of the infrastructure deployed. 
It records a mapping from the Terraform resources in templates to the representation of those resources in the real world, in JSON format.
In theory having terraform.tfstate under version control could allow multiple teammates to work the same infrastructure. We can see multiple 
problems to that solution :
- Forgetting to pull the last tfstate could lead to umpredictable results and the inability to efficiantly work on the infrastructure.
- Git or other don't lock file so 2 team members can terraform apply at the same time creating different tfstate files.
- tfstate could include sensitive information such as logins and password (db_instances for example)
The real solution to these problems is to use terraform remote backends to host the state files. Remote backends could be saved on AWS S3 for example, and encrypted if necessary.

## 2. IP 169.254.169.254
This IP address is part of dynamically configured link-local addresses (169.254.0.0/16) and this specific address is used by AWS EC2 and 
other cloud platforms to distribute metadata.
By using this IP as a service endpoint Amazon was sure that every machine could use this IP without creating address conflicts.
